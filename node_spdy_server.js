#!/usr/bin/env node

var log4js = require('log4js');
var logger = log4js.getLogger('SPDY SERVER');

var argv = require('optimist').argv;

var SPDY_SERVER_PORT  = parseInt(argv.p);
var SPDY_SERVER_PLAIN =  argv.plain ? true : false;

var http 	= require('http');
var https	= require('https');

var spdyServer = require('spdy');
var SPDY_SERVER_PORT = SPDY_SERVER_PORT;

var fs = require('fs');

var options = {
    plain: SPDY_SERVER_PLAIN
};

if (!SPDY_SERVER_PLAIN) {
  options['ssl']  = true;
  options['key']  = fs.readFileSync('./keys/spdy-key.pem');
  options['cert'] = fs.readFileSync('./keys/spdy-cert.pem');
  options['ca']   = fs.readFileSync('./keys/spdy-csr.pem');
}

var serverModule = SPDY_SERVER_PLAIN ? http.Server : https.Server;
var serverInstance = spdyServer.createServer(serverModule, options, function(req, res) {
  res.writeHead(200);
  res.end('hello world!\n');
  logger.info('Handled SPDY request!');
});

serverInstance.listen(SPDY_SERVER_PORT);
