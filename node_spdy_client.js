#!/usr/bin/env node

var argv = require('optimist').argv;

var SPDY_SERVER_HOST          = argv.h;
var SPDY_SERVER_PORT          = parseInt(argv.p);
var SPDY_CONNECTION_PLAIN     = argv.plain ? true : false;
var NB_SIMULTANEOUS_REQUESTS  = argv.requests || 4;

var log4js = require('log4js');
var logger = log4js.getLogger('SPDY CLIENT');

var spdyClient = require('spdy-client');
spdyClient.setLogLevel('DEBUG');

reconnectToSpdyServer();

function reconnectToSpdyServer() {
  logger.info('Reconnecting to SPDY server...');

  spdyClient.flushConnection(SPDY_SERVER_HOST, SPDY_SERVER_PORT, SPDY_CONNECTION_PLAIN);
  var newConnection = connectToSpdyServer();

  if (newConnection) {
    newConnection.once('close', function() {
      reconnectToSpdyServer();
    });

    newConnection.once('connected', function() {
      startSendingSpdyRequests();
    });
  }

}

function connectToSpdyServer() {
  var spdyClientConnection = spdyClient.getConnection(SPDY_SERVER_HOST,
                                                      SPDY_SERVER_PORT,
                                                      SPDY_CONNECTION_PLAIN);
  return spdyClientConnection;
}

function startSendingSpdyRequests() {
  logger.info('Starting to send SPDY requests to server...');
  for (var i = 0; i < NB_SIMULTANEOUS_REQUESTS; ++i) {
    sendSpdyGetRequest();
  }
}

function sendSpdyGetRequest() {
  var req = spdyClient.get(
    {
      method: 'GET',
      path  : '/',
      url   : '/',
      port  : SPDY_SERVER_PORT,
      host  : SPDY_SERVER_HOST,
      plain : SPDY_CONNECTION_PLAIN
    },
    function(response) {
      logger.info("--- GET  RESPONSE --");
      response.once('data', function (chunk) {
        var data = String.fromCharCode.apply(null, new Uint16Array(chunk));
        logger.info(data);
      });

     sendSpdyGetRequest();
    });

  if (req) {
    req.on('error', function(err){
          console.log('Error in sendSpdyGetRequest');
          logger.error(err);
    });
  }
}
